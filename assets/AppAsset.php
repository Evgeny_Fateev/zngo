<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/bootstrap.min.css',
        'css/font-awesome.css',
        'css/menuzord.css',
        'css/stroke-icon.css',
        'css/demo.css',
        'css/style.css',
        'css/ie7.css',
        'css/bootFolio.css',
        'css/magnific-popup.css',
        'css/jquery-ui.css',
        'css/owl.theme.default.css',
        'revolution/css/settings.css',
        'revolution/css/layers.css',
        'revolution/css/navigation.css',
    ];
    public $js = [

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
