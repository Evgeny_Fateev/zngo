<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 31.07.2017
 * Time: 12:49
 */

namespace app\controllers;


use app\models\News;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class BehaviorsController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'controllers' => ['home'],
                        'actions' => ['add-news'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['home'],
                        'actions' => ['news','update','delete'],
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['home'],
                        'actions' => ['personal-area'],
                        'roles' => ['@'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}