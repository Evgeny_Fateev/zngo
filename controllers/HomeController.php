<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 31.07.2017
 * Time: 12:53
 */

namespace app\controllers;


use app\models\News;
use app\models\User;
use Yii;
use yii\web\UploadedFile;

class HomeController extends BehaviorsController
{
    public function actionAddNews(){
        $this->view->title = "Добавление новости";
        $news = new News();
        if ($news->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isPost){
                $news->image = UploadedFile::getInstance($news, 'image');
                $news->image = $news->upload();

            }
            $news->save();
            $this->goHome();
        }
        return $this->render('add-news', compact('news'));
    }

    public function actionNews(){
        $news = News::find()->all();
        return $this->render('news', compact('news'));
    }
    public function actionDelete($id){
        $news = News::findOne($id)->delete();
        $this->redirect('news');
    }
    public function actionUpdate($id){
        $news = News::find()->where(['id' => $id])->one();
        if ($news->load(Yii::$app->request->post())) {
            $news->image = UploadedFile::getInstance($news, 'image');
            $news->image = $news->upload();
            $news->save();
            $this->redirect('news');
        }
        return $this->render('update', compact('news'));

    }
    public function actionPersonalArea(){
        $this->view->title = 'Личный кабинет';
        echo 'Личный кабинет';
        return $this->render('personal-area');
    }


}