<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 01.08.2017
 * Time: 10:16
 */

namespace app\controllers;


use yii\web\Controller;

class PageController extends Controller
{
    public $layout = '@app/views/layouts/zngo.php';
    public function actionAbout(){

        return $this->render('about');
    }
    public function actionFaq(){

        return $this->render('faq');
    }
    public function actionContact(){

        return $this->render('contact');
    }
    public function actionCooperation(){
        return $this->render('cooperation');
    }
    public function actionSingleBlog(){
        return $this->render('single-blog');
    }
}