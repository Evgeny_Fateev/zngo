<?php

namespace app\controllers;

use app\models\News;
use app\models\SignupForm;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/zngo.php';
        $news = News::find()->all();

        return $this->render('index', compact('news'));
    }




    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm','id'=>$user->id,'key'=>$user->auth_key]);
                $email = \Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Подтверждение E-mail')
                    ->setTextBody("Для подтверждения почты нажмите на ссылку " .$url)
                    ->send();
                if($email){
                    Yii::$app->getSession()->setFlash('success','Письмо для подтверждения отправлено Вам на почту!');
                }
                else{
                    Yii::$app->getSession()->setFlash('warning','Ошибка!');
                }
                return $this->refresh();
            }
        }

        return $this->render('signup', ['model' => $model,]);
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionAddAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = '123@m.ru';
            $user->setPassword('123');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }
    public function actionConfirm($id, $key)
    {
        $model = User::find()->where([
            'id'=>$id,
            'auth_key'=>$key,
            'status'=>0,
        ])->one();
        if(!empty($model)){
            $model->status=10;
            $model->save();
            Yii::$app->getSession()->setFlash('success','E-mail подтвержден!');
        }
        else{
            Yii::$app->getSession()->setFlash('warning','Ошибка!');
        }
        return $this->goHome();
    }
}
