<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 31.07.2017
 * Time: 12:58
 */

namespace app\models;


use yii\db\ActiveRecord;

class News extends ActiveRecord
{
    public static function tableName()
    {
        return 'news';
    }


    public function attributeLabels()
    {
        return [
            'title' => 'Заголовок',
            'image' => 'Изображение',
            'text' => 'Текст',
            'category' => 'Категория',
        ];
    }
    public function rules()
    {
        return [
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['title', 'image', 'text','category'], 'required'],
        ];
    }
    public function upload(){
        if($this->validate()){
            $hash_name = md5(microtime() . rand(0, 9999));
            $this->image->saveAs("images/{$hash_name}.{$this->image->extension}");
            return "images/{$hash_name}.{$this->image->extension}";
        }
        else{
            return false;
        }
    }

    public static function getNextOrPrevId($currentId, $nextOrPrev)
    {
        $records=NULL;
        if($nextOrPrev == "prev")
            $order="id DESC";
        if($nextOrPrev == "next")
            $order="id ASC";

        $records = News::find(
            array('select'=>'id', 'order'=>$order)
        );

        foreach($records as $i=>$r)
            if($r->id == $currentId)
                return isset($records[$i+1]->id) ? $records[$i+1]->id : NULL;

        return NULL;
    }
}