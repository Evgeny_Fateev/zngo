<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
?>
<div class="panel panel-default">
    <div class="panel-body">
        <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
        <?= $form->field($news, 'title')->textInput();?>
        <?= $form->field($news, 'image')->fileInput();?>
        <?= $form->field($news, 'text')->textarea();?>
        <?= $form->field($news, 'category')->radioList(['Блог'=> 'Блог', 'Новость' => 'Новость']);?>
        <?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
        <?php  ActiveForm::end(); ?>
    </div>
</div>