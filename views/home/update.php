<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="col-md-3">
    <div class="text-center">
        <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
        <?= $form->field($news, 'title')->textInput();?>
        <div style="width: 220px; height: 180px;">
            <? echo Html::img('@web/'.$news->image, ['style' =>'max-width:100%;max-height: 100%; border: 1px solid black'])?>
        </div>
        <?= $form->field($news, 'image')->fileInput();?>
        <div class="text-center" >
            <?= $form->field($news, 'text')->textInput();?>
        </div>
        <?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
        <?php  ActiveForm::end(); ?>


    </div>
</div>

