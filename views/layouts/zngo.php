<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Завод нефтегазового оборудования</title>
    <!--     FAVICON-->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Bootstrap -->
    <link href="/web/css/bootstrap.min.css" rel="stylesheet">
    <!--FONT AWESOME -->
    <link href="/web/css/font-awesome.css" rel="stylesheet">
    <!-- MENU -->
    <link rel="stylesheet" href="/web/css/menuzord.css">
    <!-- STROKE GAP ICON -->
    <link href="/web/css/stroke-icon.css" rel="stylesheet">
    <link href="/web/css/demo.css" rel="stylesheet">
    <link href="/web/css/ie7.css" rel="stylesheet">
    <!-- Portfolio Filter -->
    <link rel="stylesheet" href="/web/css/bootFolio.css">
    <!-- Popup -->
    <link rel="stylesheet" href="/web/css/magnific-popup.css">
    <!-- JQUERY UI STYLE -->
    <link rel="stylesheet" href="/web/css/jquery-ui.css">
    <!-- OWL CSS -->
    <link href="/web/css/owl.theme.default.css" rel="stylesheet">
    <link href="/web/css/owl.carousel.css" rel="stylesheet">
    <!-- RS5.0 Main Stylesheet -->
    <link rel="stylesheet" type="text/css" href="/web/revolution/css/settings.css">
    <!-- RS5.0 Layers and Navigation Styles -->
    <link rel="stylesheet" type="text/css" href="/web/revolution/css/layers.css">
    <link rel="stylesheet" type="text/css" href="/web/revolution/css/navigation.css">
    <!-- MAIN STYLE -->
    <link rel="stylesheet" href="/web/css/style.css">

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file: -->

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->


    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="outslider_loading">
    <div class="outslider_loader"></div>
</div>
<header class="header-area navbar-fixed-top">
    <div class="container custom-header">
        <div class="row">
            <div id="menuzord" class="menuzord">

                <div class="header-search">

                    <div class="add-to-cart-content">
                        <div class="recent-post shop-right-thumb add-cart-thumb">
                            <h3><a href="#">Personal Branding</a></h3>
                            <p> 1 x $ 65.00</p>
                            <img src="/web/images/shop-right-book-1.png" alt="">
                            <span><a href="#"><img src="/web/images/shop-cart-cross.png" alt=""></a></span>
                        </div>
                        <div class="recent-post shop-right-thumb add-cart-thumb">
                            <h3><a href="#">Marketing</a></h3>
                            <p>1 x $ 65.00</p>
                            <img src="/web/images/shop-right-book-1.png" alt="">
                            <span><a href="#"><img src="/web/images/shop-cart-cross.png" alt=""></a></span>
                        </div>
                        <p>Subtotal:<span>$145.00</span></p>
                        <button type="button" class="btn btn-dm">View Cart</button>
                        <button type="button" class="btn btn-dm">Checkout</button>
                    </div>
                    <div class="search-content">
                        <form action="#">
                            <input type="text" name="googlesearch" placeholder="Type Your Search Words">
                            <input type="submit" id="searchsubmit" value="Search">
                        </form>
                    </div>
                </div>
                <ul class="menuzord-menu menuzord-menu-bg">
                    <li class="active"><a href="/">Главная</a></li>
                    <li><a href="/page/about">О заводе</a></li>
                    <li><a href="/page/faq">Вопрос - ответ</a></li>
                    <li><a href="/blog-grid.html">Как купить</a></li>
                    <li><a href="/page/cooperation">Сотрудничество</a></li>
                    <li><a href="/page/contact">Контакты</a></li>
                </ul>

                <div class="header_right-block">
                    <div class="appointment-area">
                        <p>
                            <select class="form-control" onChange="if(this.options[this.selectedIndex].value!=''){window.location=this.options[this.selectedIndex].value}else{this.options[selectedIndex=0];}">
                                <option value="">Language</option>
                                <option><?= Html::a('fr', ['/', 'language' => 'fr']) ?></option>
                                <option><?= Html::a('de', ['/', 'language' => 'de']) ?></option>
                                <option><?= Html::a('ar', ['/', 'language' => 'ar']) ?></option>
                                <option><?= Html::a('zh-CN', ['/', 'language' => 'zh-CN'])?></option>
                                <option><?= Html::a('ru', ['/', 'language' => 'ru']) ?></option>
                                <option><?= Html::a('ita', ['/', 'language' => 'ita']) ?></option>
                                <option><?= Html::a('en', ['/', 'language' => 'en']) ?></option>
                            </select>



                    </div>
                    <a href="<?=\yii\helpers\Url::to(['/home/personal-area'])?>" class="header_user">
                        <img src="/web/images/site-content/user.png" alt="">
                        <span class="user_home">Личный кабинет</span>
                    </a>
                    <a href="#" class="header_basket">
                        <img src="/web/images/site-content/basket.png" alt="">
                        <span class="header-basket-text">Корзина</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</header>
<?= $content ?>
<?php $this->endBody() ?>

<footer class="footer-area">


    <div class="footer-bottom-content-details">
        <div class="container-fluid">
            <div class="row">
                <div class="footer-bottom-content">
                    <div class="col-md-9 col-sm-9 no-padding">
                        <div class="footer-bottom-single">
                            <div class="footer-menu">
                                <a href="/" class="footer_link-navigation">Главная</a>
                                <a href="/page/about" class="footer_link-navigation">О заводе</a>
                                <a href="/page/faq" class="footer_link-navigation">Вопрос- ответ</a>
                                <a href="#" class="footer_link-navigation">Как купить </a>
                                <a href="/page/cooperation" class="footer_link-navigation">Сотрудничество</a>
                                <a href="/page/contact" class="footer_link-navigation">Контакты</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 no-padding">
                        <div class="footer-bottom-single text-center">
                            <p><a href="http://themeforest.net/user/7oroof/portfolio?ref=7oroof" class="access_link">2017 Условия пользования сайтом</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="/web/js/jquery-1.11.3.min.js"></script>
<!-- RS5.0 Core JS Files -->
<script type="text/javascript" src="/web/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>
<script type="text/javascript" src="/web/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>
<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="/web/js/bootstrap.min.js"></script>
<script src="/web/js/menuzord.js"></script>
<script src="/web/js/owl.carousel.min.js"></script>
<script src="/web/js/jquery.counterup.js"></script>
<script src="/web/js/waypoints.min.js"></script>
<script src="/web/js/countdown.js"></script>
<script src="/web/js/parallax.min.js"></script>
<script src="/web/js/jquery.bootFolio.js"></script>
<script src="/web/js/jquery.magnific-popup.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script src="/web/js/jquery-ui.js"></script>
<script src="/web/js/rev-function.js"></script>
<script src="/web/js/smoothscroll.js"></script>
<script src="/web/js/jquery.matchHeight.js"></script>
<script src="/web/js/main.js"></script>
</body>

<?php $this->endPage() ?>
