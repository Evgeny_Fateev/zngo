<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Завод нефтегазового оборудования</title>
    <!-- FAVICON -->


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- =========================
  START PRELOADER
============================== -->
<div class="outslider_loading">
    <div class="outslider_loader"></div>
</div>


<div class="page-title-area about-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2>О Заводе</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/">Главная</a></li>

                        <li><a href="/page/about">О Заводе</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->


<!-- =========================
  START TEAM SECTION
============================== -->
<section class="team-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>ПРОЕКТНАЯ</h3>
                        <h2>КОМАНДА</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="team-area-details">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-1.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Ahmed Abd-Alhaleem</h2>
                                <p>Head of Investment</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Omar Elnagar</h3>
                            <p>Civil Engineer</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-2.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Ahmed Hassan</h2>
                                <p>Tax Advice</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Ahmed Hassan</h3>
                            <p>Tax Advice</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-3.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Mohamed Habaza</h2>
                                <p>Business Planner</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Mohamed Habaza</h3>
                            <p>Business Planner</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-4.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Amr Gamal Sadeq</h2>
                                <p>Stock Market Broker</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Amr Gamal Sadeq</h3>
                            <p>Stock Market Broker</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

            </div>
        </div>
    </div>
</section>
<!-- =========================
  END TEAM SECTION
============================== -->

<!-- =========================
  START ACCORDION 2 SECTION
============================== -->
<section class="accordion-area bg-type-2">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">

                        <h2>Описание</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="accordion-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion-content-inner">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            ZNGOcoin
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        Это первая в своём роде криптовалюта, которая надежно подкреплена промышленным производством. Средства инвесторов, вложенные в рост этого блокчейна, идут на развитие ООО «Завод нефтегазового оборудования». Завод производит детали трубопроводов, шаровые краны, клиновые и шиберные задвижки.
                                        <br><br>ЗНГО (Завод Нефтегазового Оборудования) прошёл аккредитацию в компании «Газпром» и сейчас планирует увеличить штат своих сотрудников на 150–180 человек, чтобы выйти на более высокий уровень производства. Валюта ZNGOcoin подкрепляется оборудованием, зданиями и контрактами. На ранних этапах развития криптовалюты инвесторы, вкладывающие капитал в ZNGOcoin, получат до 100% прибыли.
                                        <br><br>Криптовалюта ZNGOcoin в своём роде уникальна. Активы, которые ее подкрепляют, вполне материальны и одновременно с этим максимально защищены от влияния случайных внешних факторов. Специфика деятельности Завода Нефтегазового Оборудования определяет потенциал быстрого роста криптовалюты.

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Чем занимается ЗНГО?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Завод Нефтегазового Оборудования является предприятием полного цикла и изготавливает трубопроводную арматуру. Сотрудники предприятия продолжают совершенствовать свои знания и навыки, а руководство принимает решения о модернизации и расширении производства. Создание ZNGOcoin`a - это новый виток развития завода. Он приведет к  увеличению парка оборудования, созданию новых рабочих мест и повышению собственного престижа на мировом рынке.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Почему ZNGOcoin — это надёжно?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Государственныые валюты волатильны и не всегда стабльны. Их курс может падать из-за политических разногласий, торговых неудач страны, социальных кризисов и многих других причин. В итоге, капиталы и сбережения в государственной валюте могут резко упасть в цене или даже полностью обесцениться.
                                        <br><br>ZNGOcoin — совершенно новый информационный и инвестиционный продукт. Он сочетает в себе все преимущества блокчейна, поэтому не может быть умышленно дискредитирован. Материальную поддержку ZNGOcoin составляют станки и оборудование завода, а так же заключённые заводом контракты (актуальные и потенциальные). Успех завода положительно влияет на стоимость криптовалюты и определяет темпы ее роста. Инвесторы ZNGOcoin могут рассчитывать на гарантированную прибыль, которая на ранних этапах может доходить до 100% от суммы вложений.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="quote-4-slider" class="owl-carousel all-carousel owl-theme">
                        <div class="quote-single">
                            <div class="quote-main-content">
                                <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                                <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                                <p>
                                    <b>Миссия ООО «ЗНГО»</b>
                                <p>
                                <ul style="font-size: 14px;">
                                    <li>Поставлять на рынок продукцию безупречного качества, которая позволит решать проблемы газовой и нефтяной отрасли.</li>
                                    <li>Осуществлять эти поставки на рынок при помощи различных методов и энергичных работников, обладающих талантами и навыками, необходимыми для поддержания репутации ООО «Завод нефтегазового оборудования», как ведущего отечественного производителя запорной арматуры и соединительных деталей трубопровода.</li>
                                    <li>Выполнять нашу фундаментальную задачу, связанную с полным удовлетворением Потребителей.</li>
                                    <li>Совершенствовать конструкцию и улучшать качество выпускаемой продукции.</li>
                                </ul>

                                </p>


                                </p>
                            </div>
                            <div class="quote-author">
                                <img src="/web/images/quote-author-4.jpg" alt="">
                                <div class="quote-author-details">
                                    <h3>Иван Иванович</h3>
                                    <p>Главный инженер</p>
                                </div>
                            </div>
                        </div>
                        <div class="quote-single">
                            <div class="quote-main-content">
                                <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                                <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                                <p>M<iframe width="418" height="315" src="https://www.youtube.com/embed/iwwsYFhSpok?list=PLuISL7H-gmBF2WiojMdiHHEqvHILyzMf4" frameborder="0" allowfullscreen></iframe></p>
                            </div>
                            <div class="quote-author">
                                <img src="/web/images/quote-author-2.jpg" alt="">
                                <div class="quote-author-details">
                                    <h3>James Norway</h3>
                                    <p>Art Director</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END ACCORDION 2 SECTION
============================== -->

<!-- =========================
  START SERVICE SECTION
============================== -->
<section class="service-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">

                        <h2>Сертификаты</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="service-content-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 no-padding">
                    <div id="service-2-slider" class="owl-carousel all-carousel owl-theme">
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-1.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-2.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-3.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-1.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-2.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-3.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- =========================
  END SERVICE SECTION
============================== -->

<!-- =========================
  START COUNTER UP SECTION
============================== -->
<section class="counter-up-area home-6-counter-up-area">
    <div class="container">
        <div class="row">
            <div class="counter-up-content">
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">5000</span></h2>
                        <p>Million Dollars Saved</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">1325</span></h2>
                        <p>Successful Deals</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">321</span></h2>
                        <p>Advisors & Experts</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner w-r-l-border">
                        <h2><span class="counter">2314</span></h2>
                        <p>Happy Customers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END COUNTER UP SECTION
============================== -->

<!-- =========================
  START BLOG SECTION
============================== -->

<!-- =========================
  END BLOG SECTION
============================== -->


<!-- =========================
  START QUOTE SECTION
============================== -->
<section class="client-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">

                        <h2>НАШИ ПАРТНЁРЫ</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="client-slider" class="owl-carousel all-carousel owl-theme">
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-1.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-5.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-2.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-6.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-3.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-7.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-4.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-8.png" alt=""></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END QUOTE SECTION
============================== -->

<!-- =========================
  START SIGN UP SECTION
============================== -->
<section class="sign-up-area parallax-window" data-parallax="scroll" data-image-src="./images/sign-up-bg.png">
    <div class="container">
        <div class="row">
            <div class="sign-up-content">
                <div class="col-sm-9">
                    <div class="sign-up-left">
                        <span>Готовы стать инвестором?</span>

                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sign-up-btn">
                        <a href="#">Купить ZNGOcoin?</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  START SIGN UP SECTION
============================== -->


</body>
</html>
