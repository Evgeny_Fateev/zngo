<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Завод нефтегазового оборудования</title>
    <!-- FAVICON -->


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="page-title-area contact-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2>Свяжитесь с нами</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/">Главная</a></li>
                        <li><a href="/page/contacts">Контакты</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->

<!-- =========================
START CONTACT US SECTION
============================== -->
<section class="contact-us-area contact-us-1-area">
    <div class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="contact-form-3-text">
                        <h2>Контактная информация</h2>
                        <p>

                        </p>
                        <div class="address-area">
                            <div class="view-location">
                                <h2>Адрес :</h2>
                                <p><i class="fa fa-map-marker"></i>Россия, 644901, г. Омск, мкр. Береговой, ул. Иртышская, 1 «А»</p>
                                <a href="#">Показать на карте</a>
                            </div>
                            <div class="col-md-6 no-padding-left">
                                <div class="address-details">
                                    <h2>Email :</h2>
                                    <span><i class="fa   fa-envelope"></i>7oroof@7oroof.com</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding-left">
                                <div class="address-details">
                                    <h2>Телефон :</h2>
                                    <span><i class="fa  fa-phone"></i> +7 (3812) 98 19 09</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="contact-form-left contact-form-3 contact-form-right">
                        <h2>Напишите нам</h2>
                        <div class="show_result"></div>
                        <div class="result_message"></div>
                        <form>
                            <div class="row">
                                <div class="col-sm-6">
                                    <input type="text" id="Name" class="form-control" placeholder="Имя">
                                </div>
                                <div class="col-sm-6">
                                    <input type="email" id="Email" class="form-control" placeholder="Email">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="Phone" placeholder="Телефон">
                                </div>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" id="Subject" placeholder="Тема">
                                </div>
                                <div class="col-sm-12">
                                    <textarea class="form-control" rows="3" id="Message"  placeholder="Сообщение"></textarea>
                                </div>
                                <div class="col-sm-12 text-center">
                                    <button type="button" id="contact_submit" class="btn btn-dm">Отправить сообщение</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
START CONTACT US SECTION
============================== -->
<div id="map"></div>

<!-- =========================
  START FOOTER SECTION
============================== -->

<script src="/web/js/map.js"></script>
</body>
</html>



