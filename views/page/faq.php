<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Завод нефтегазового оборудования</title>

    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lora:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!-- =========================
  END HEADER SECTION
============================== -->

<!-- =========================
  START PAGE TITLE SECTION
============================== -->
<div class="page-title-area faq-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-8">
                <div class="page-title-left">
                    <h2>Ответы на частые вопросы</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6 col-lg-4">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/">Главная</a></li>

                        <li><a href="/page/faq">Вопрос - ответ</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->

<!-- =========================
  START SERVICE SECTION
============================== -->
<section class="service-area service-page-section">
    <div class="service-content-area">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <div class="accordion-content-inner faq-accordion-content-inner">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            Что такое ЗНГО?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        ЗНГО это завод нефтегазового оборудования, который имеет богатую историю, которая берет свое начало более 15 лет назад. ЗНГО является уникальным заводом, с точки зрения как импортозамещения, так и со стороны качества и того что он обеспечивает полный цикл изготовления своей продукции.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Для чего используется продукция ЗНГО?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        Продукция ЗНГО используется в нефтегазовой отрасли, для транспортировки углеводородов а так же в системах водоснабжения город и районных центров Российской Федерации а так же ближнего и дальнего зарубежья. В данный момент перспективная отрасль применения задвижек и шаровых кранов, которые выдерживают колоссальные давления пара это атомная промышленность.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Сколько вы хотите привлечь инвестиций?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        Мы хотим привлечь более 3 миллиардов рублей (более 50 000 000 млн долларов).

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading5">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="false" aria-controls="collapse5">
                                            Чем вы можете подтвердить качество вашей продукции?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
                                    <div class="panel-body">
                                        Наша продукция подтверждена сертификатами качества и соответствия, таких компаний как Газпром, Лукойл, Транснефть, компания прошла обязательную сертификацию качества и соответствия выпускаемых изделий а так же доказанной временем репутацией на рынке.

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading6">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="false" aria-controls="collapse6">
                                            Кто может купить ZNGOcoin?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
                                    <div class="panel-body">
                                        ZNGOcoin может приобрести любой желающий у которого имеется 5 долларов или 300 рублей за 1 токен.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading7">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="false" aria-controls="collapse7">
                                            Какие гарантии вы даёте инвесторам ?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
                                    <div class="panel-body">
                                        Наши гарантии - наше имя, мы не скрываем ничего от инвесторов, у нас были как как взлеты так и падения, но мы справились со всем этим, компания несмотря на все трудности нынешней экономической ситуации в мире продолжает расти и развиваться, мы обеспеченны контрактами, мы даем инвесторам обеспечения их вложений материальными активами такими как : оборудование, здания цехов и корпусов, земельные участки завода, транспортные средства, произведенное оборудование и наши разработки. А так же профессиональные работники данной отрасли, которые имеют богатый и длительный опыт работы на нашем заводе и являются основополагающим фактором качества производимой продукции.
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading9">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="false" aria-controls="collapse9">
                                            Если не получится собрать необходимeю сумму через ICO?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
                                    <div class="panel-body">
                                        Если не получится собрать необходимую сумму то мы продолжим развиваться своими силами, шаг за шагом, будем дальше продолжать работать и стремиться к достижению поставленной цели а для этого у нас есть все необходимое.

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading10">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                            Что будете делать после получения необходимой суммы?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
                                    <div class="panel-body">
                                        После получения необходимой суммы, мы сразу же начнем реализовывать проекты которые описаны в бизнес плане, в максимально сжатые сроки этап за этапом мы запустим цеха, сможем брать больше контрактов для освоения и, как следствие, начать выплату дивидендов и возврат средств в рамках ICO.

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading11">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                            Почему решили воспользоваться привлечением инвестиций через ICO?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
                                    <div class="panel-body">
                                        Мы следим за современными технологиями и трендами, и считаем, что это будущее взаимоотношений между людьми, мы хотим чтобы инвестором мог выступить любой человек который верит в наше начинание, в наш завод, и помочь нам в развитие, чтобы в будущем, не только мы, но и инвесторы были обеспеченны и уверены в завтрашнем дне! Мы уникальны как компания которая выходит на рынок криптовалюты и блокчейн технологий, так как даем в качестве обеспечения продукцию связанную с нефтяной промышленностью, то без чего добыча углеводородов не представляется возможным, и именно поэтому, давая уверенность, являясь уникальным проектом а так же имея действующие долгосрочные и перспективные контракты и доброе имя, руководство приняло решение воспользоваться таким способом привлечения средств как ICO.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="service-left-sidebar">
                        <h2>Документы</h2>
                        <p>Подробное описание нашей компании и виденье будущего вы можете прочитать в нашем бизнес - плане и Whate Paper</p>
                        <a href="#">
                            <span>PDF</span>
                            <span>БИЗНЕС - ПЛАН</span>
                            <i class="fa fa-download"></i>
                        </a>
                        <a href="#">
                            <span>PDF</span>
                            <span>WHITE PAPER</span>
                            <i class="fa fa-download"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END SERVICE SECTION
============================== -->

<!-- =========================
  START SIGN UP SECTION
============================== -->
<section class="sign-up-area parallax-window" data-parallax="scroll" data-image-src="./images/sign-up-bg.png">
    <div class="container">
        <div class="row">
            <div class="sign-up-content">
                <div class="col-sm-9">
                    <div class="sign-up-left">

                        <span> Готовы стать инвестором?</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sign-up-btn">
                        <a href="#">Купить ZNGOcoin</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  START SIGN UP SECTION
============================== -->




</body>
</html>
