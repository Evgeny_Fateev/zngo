<?php
use yii\helpers\Html;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;
use app\widgets\LoginFormWidget;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Experts</title>


    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>


<div class="page-title-area blog-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2>Blog Single Post</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/">Home</a></li>
                        <li><a href="/page/single-blog">Single Post</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->

<!-- =========================
  START BLOG SECTION
============================== -->
<section class="blog-area blog-page-area bg-type-2">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">

                <div class="single-blog-content-area">
                    <div class="blog-content-single blog-content-single-no-btm-padding">
                        <div class="blog-img">
                            <? echo Html::img('@web/'.$blogs->image)?>
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li><?=$blogs->date ?> </li>
                                <li>
                                    <a href="#"><?=$blogs->category?></a>
<!--                                    <a href="#"> Bills</a>-->
                                </li>
                                <li>Comments</li>
                                <li>
                                    <a href="#">80</a>
                                </li>
                            </ul>
                            <h2><?=$blogs->title?></h2>
                            <p><?=$blogs->text ?></p>

                            <div class="blog-social text-left">
                                <p><span>Share This Artcle : </span>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                    <a href="#"><i class="fa fa-pinterest"></i></a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="blog-prev-next clearfix">
                    <div class="blog-prev">
                        <div class="blog-prev-content">
                            <p>Previous Post</p>
                            <h3><a href="<?= yii\helpers\Url::to (['page/single-blog', 'id' => $blogs_prew])?>"><?$blogs_prew->title?>fvfbf</a></h3>
                            <div class="prev-left">
                                <a href="#"><img src="/web/images/blog-siderbar-author-1.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="blog-next text-right">
                        <div class="blog-next-content">
                            <p>Next Post</p>
                            <h3><a href="<?= yii\helpers\Url::to (['page/single-blog', 'id' => $blogs_next])?>">Scraping and Cleaning Your Data with Google Sheets</a></h3>
                            <div class="next-right">
                                <a href="#"><? echo Html::img('@web/'.$related_blog->image)?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="blog-siderbar-area single-blog-author">
                    <h2>About Author</h2>
                    <div class="single-blog-author-post">
                        <p>we’re Metrics, an award-winning digital marketing agency based in New York. Founded by Begha over many cups of tea at her kitchen table in 2009, our brand promise is simple: to provide powerful digital marketing solutions to small and medium businesses. </p>
                        <img src="/web/images/single-blog-author-1.png" alt="">
                        <div class="blog-social  blog-social-2 blog-social-padding-btm">
                            <p>
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>

                            </p>
                        </div>
                    </div>
                </div>
                <div class="blog-siderbar-area single-blog-thumbnail single-blog-thumbnail-c-padding clearfix">
                    <h2>Related Posts</h2>
                    <?php foreach ($related_blogs as $related_blog):?>
                    <div class="col-sm-12 col-sm-4 col-md-4 ">
                        <div class="blog-content-single blog-content-single-no-m-btm">
                            <div class="blog-img" style="width: 209px; height: 167px;">
                                <? echo Html::img('@web/'.$related_blog->image, ['style' =>'max-width:100%;max-height: 100%'])?>
                                <i class="fa fa-image"></i>
                            </div>
                            <div class="blog-text">
                                <ul>
                                    <li><?=$related_blog->date?> </li>
                                </ul>
                                <h2><a href="<?= yii\helpers\Url::to (['page/single-blog', 'id' => $related_blog->id])?>"><?=$related_blog->title?></a></h2>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>

<!--                    <div class="col-sm-12 col-sm-4 col-md-4">-->
<!--                        <div class="blog-content-single blog-content-single-no-m-btm">-->
<!--                            <div class="blog-img">-->
<!--                                <img src="/web/images/blog/blog-3.png" alt="" class="img-responsive">-->
<!--                                <i class="fa fa-vimeo-square"></i>-->
<!--                            </div>-->
<!--                            <div class="blog-text">-->
<!--                                <ul>-->
<!--                                    <li>Feb 22, 2016 </li>-->
<!--                                </ul>-->
<!--                                <h2><a href="#">In the news: this week’s top money stories</a></h2>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <div class="blog-siderbar-area single-blog-thumbnail clearfix">
                    <h2>3 Comments</h2>
                    <div class="single-blog-author-post single-blog-reply-post">
                        <div class="blog-reply-content">
                            <h3><a href="#">Mohamed Habaza</a></h3>
                            <h4>Jul 8, 2015 - 08:07 pm</h4>
                            <p>Perfecting website optimization is already complicated thing, yet venturing to uncharted sees of off page SEO isn't only time consuming yet unbelievably mind racking. However, no matter how I look at it, perfect web optimization is too much for a filler statement. </p>
                            <img src="/web/images/blog-comment-author-1.png" alt="">
                            <a href="#">Reply <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog-author-post single-blog-reply-post blog-author-left-margin">
                        <div class="blog-reply-content">
                            <h3><a href="#">Ahmed Hassan</a></h3>
                            <h4>Jul 8, 2015 - 08:07 pm</h4>
                            <p>The example about the mattress sizing page you mentioned in the last WBF can be a perfect example of new keywords and content, and broadening the funnel as well. I can only imagine the sale numbers if that was the site of a mattress selling company.</p>
                            <img src="/web/images/blog-comment-author-2.png" alt="">
                            <a href="#">Reply <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                    <div class="single-blog-author-post single-blog-reply-post">
                        <div class="blog-reply-content b-l-no-border">
                            <h3><a href="#">Ahmed Hassan</a></h3>
                            <h4>Jul 8, 2015 - 08:07 pm</h4>
                            <p>The example about the mattress sizing page you mentioned in the last WBF can be a perfect example of new keywords and content, and broadening the funnel as well. I can only imagine the sale numbers if that was the site of a mattress selling company.</p>
                            <img src="/web/images/blog-comment-author-3.png" alt="">
                            <a href="#">Reply <i class="fa fa-long-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <?= (Yii::$app->user->isGuest ? LoginFormWidget::widget([]) : ''); ?>
                <div class="blog-siderbar-area single-blog-thumbnail clearfix">
                    <h2>Leave A Reply</h2>
                    <div class="blog-reply-form">


                        <a data-target="#login-modal" data-toggle="modal" href="#">Вход</a>
                        <?php if(Yii::$app->user->isGuest):  ?>
                            <h3>Для оставления комментариев пожалуйста <a href="/site/ajax-login">авторизуйтесь</a>
                                или <a href="/site/signup">зарегистрируйтесь</a></h3>
                        <?php endif;?>
                        <?php if(!Yii::$app->user->isGuest):  ?>

                        <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id'=>'newsletter-signup']]);?>

                            <input type="email" class="form-control" id="Email" placeholder="<?echo $fvfb?>">

                        <?= $form->field($comment, 'comment')->textarea(
                            ['placeholder'=>'Comment', 'id'=>'Message'])->label(false);?>
                        <button type="submit" class="btn btn-dm">Post Your Comment</button>
                        <?php ActiveForm::end();?>
                        <?php endif;?>
<!--                        <form>-->
<!--                            <input type="text" class="form-control" id="Name" placeholder="YOUR NAME">-->
<!--                            <input type="email" class="form-control" id="Email" placeholder="EMAIL">-->
<!--                            <input type="text" class="form-control"  placeholder="WEBSITE">-->
<!--                            <textarea class="form-control" rows="8" id="Message" placeholder="COMMENT"></textarea>-->
<!--                            <button type="button" id="contact_submit" class="btn btn-dm">Post Your Comment</button>-->
<!--                        </form>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


</body>
</html>
