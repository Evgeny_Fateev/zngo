<?php
use yii\helpers\Html;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Завод нефтегазового оборудования | ZNGOcoin</title>

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <!--[endif]-->
</head>
<body>


<div class="rev_slider_wrapper rev_slider_wrapper-1">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>	<!-- SLIDE  -->
            <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <iframe width="100%" height="800" src="https://www.youtube.com/embed/OSVunY8r2KE" frameborder="0" allowfullscreen></iframe>

                <!--<img src="/images/rev-slider1.jpg"  alt="" title="bg-slider1"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS
                <div class="tp-caption   tp-resizeme  ft-droid-b-i"
                    id="slide-1-layer-1"
                    data-x="center" data-hoffset=""
                    data-y="320"
                    data-width="['auto']"
                    data-height="['auto']"
                    data-transform_idle="o:1;"
                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:500;e:easeOutCirc;"
                    data-transform_out="y:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                    data-start="700"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    style="z-index: 5; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 400; color: rgba(229, 228, 228, 1.00);">И снова текст</div>

                <!-- LAYER NR. 2
                <div class="tp-caption   tp-resizeme"
                    id="slide-1-layer-2"
                    data-x="center" data-hoffset=""
                    data-y="368"
                    data-width="['auto']"
                    data-height="['auto']"
                    data-transform_idle="o:1;"
                    data-transform_in="x:50px;opacity:0;s:500;e:easeOutCubic;"
                    data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                    data-start="900"
                    data-splitin="chars"
                    data-splitout="none"
                    data-responsive_offset="on"
                    data-elementdelay="0.1"
                    style="z-index: 6; white-space: nowrap; font-size: 55px; line-height: 50px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">Текст описывающий продукт компании </div>

                <!-- LAYER NR. 3
                <div class="tp-caption   tp-resizeme"
                    id="slide-1-layer-3"
                    data-x="center" data-hoffset=""
                    data-y="437"
                    data-width="['auto']"
                    data-height="['auto']"
                    data-transform_idle="o:1;"
                    data-transform_in="y:50px;opacity:0;s:500;e:easeOutCirc;"
                    data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                    data-start="3000"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    style="z-index: 7; white-space: nowrap; font-size: 17px; line-height: 26px; font-weight: 400; color: rgba(229, 228, 228, 1.00);font-family:Raleway;"><div class="text-center">Описание описание описание описание описание описание описание<br/> описание описание описание описание описание описание описание описание</div> </div>

                <!-- LAYER NR. 4
                <div class="tp-caption   tp-resizeme"
                    id="slide-1-layer-4"
                    data-x="center" data-hoffset="-137"
                    data-y="526"
                    data-width="['auto']"
                    data-height="['auto']"
                    data-transform_idle="o:1;"
                     data-transform_in="x:-50px;opacity:0;s:500;e:easeOutCirc;"
                    data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                    data-start="3300"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="#" class="btn rev-slider-btn">Изучите наш сервис</a> </div>

                <!-- LAYER NR. 5
                <div class="tp-caption   tp-resizeme"
                    id="slide-1-layer-5"
                    data-x="center" data-hoffset="98"
                    data-y="526"
                    data-width="['auto']"
                    data-height="['auto']"
                    data-transform_idle="o:1;"
                    data-transform_in="x:50px;opacity:0;s:500;e:easeOutCirc;"
                    data-transform_out="x:50px;opacity:0;s:300;s:300;"
                    data-start="3300"
                    data-splitin="none"
                    data-splitout="none"
                    data-responsive_offset="on"
                    style="z-index: 9; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="#" class="btn rev-slider-btn rev-slider-btn-2">Подробнее о нас</a>
          </div>
            </li>
            <!-- SLIDE  -->
            <li data-index="rs-24" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Home2" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/web/images/rev-slider2.jpg"  alt="" title="bg-slider2"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYER NR. 1 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-2"
                     data-x="center" data-hoffset=""
                     data-y="302"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:-50px;opacity:0;s:500;e:easeOutCubic;"
                     data-transform_out="y:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="700"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 5; white-space: nowrap; font-size: 55px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Raleway; max-width: 100%;"><div class="text-center">Слайдер, видеоролик объясняющий,<br/>что такое ZNGOCOIN</div> </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-3"
                     data-x="center" data-hoffset=""
                     data-y="448"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="1000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 6; white-space: nowrap; font-size: 17px; line-height: 26px; font-weight: 400; color: rgba(229, 228, 228, 1.00);font-family:Raleway;"><div class="text-center">Слайдер/видеоролик<br/>объясняющий, что такое LUXCOIN</div> </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-4"
                     data-x="center" data-hoffset=""
                     data-y="532"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:-50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                     data-start="1300"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="#" class="btn rev-slider-btn">Изучите наш сервис</a> </div>
            </li>
            <!-- SLIDE  -->
            <li data-index="rs-25" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Home6" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/web/images/rev-slider3.jpg"  alt="" title="bg-slider6"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>


            </li>
        </ul>
    </div>
</div><!-- END OF SLIDER WRAPPER -->

<!-- =========================
  START WELCOME SECTION
============================== -->
<section class="welcome-area">

    <!-- MAIN TITLE -->
    <div class="main-title welcome-main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="common-btn welcome-title-cmn-btn">
                        <div class="col-sm-6">
                            <div class="left-cmn-btn">
                                <a href="#" class="big-buttons">БИЗНЕС - ПЛАН</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="right-cmn-btn">
                                <a href="doc/white_paper.pdf" class="big-buttons">WHITE PAPER</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">

                    <div class="main-title-content text-left">
                        <form id="newsletter-signup" action="?action=signup" method="post">
                            <fieldset class="clearfix no-padding sm-text-center">
                                <div class="signup-email-block">
                                    <h5 class="links_title text-center">подписаться на новости:</h5>
                                    <input type="text" name="signup-email" id="signup-email" placeholder="впишите свой e-mail адрес"/>

                                </div>
                                <input type="image" src="/web/images/button-podpis.png" id="signup-button" value="Подписаться!" />
                                <p id="signup-response"></p>
                            </fieldset>
                        </form>



                    </div>
                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 text-center">
                    <div class="links_block">
                        <h5 class="links_title text-center">наши соцсети:</h5>
                        <a href="#"><img src="/images/social/fb.png"/></a>
                        <a href="#"><img src="/images/social/g.png"/></a>
                        <a href="#"><img src="/images/social/in.png"/></a>
                        <a href="#"><img src="/images/social/ok.png"/></a>
                        <a href="#"><img src="/images/social/p.png"/></a>
                        <a href="#"><img src="/images/social/tw.png"/></a>
                        <a href="#"><img src="/images/social/vk.png"/></a>
                        <a href="#"><img src="/images/social/ytb.png"/></a>

                    </div>

                </div>
                <div class="col-md-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="circle-block_image text-right">
                            <img src="/web/images/site-content/circle.png" alt="" class="circle-block_image">
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 text-center-xs">
                        <h4 class="circle-block_info">принцип работы</h4>
                        <ul class="circle-block_list">
                            <li class="circle-block_element"><p class="circle-block-element_number">1</p><span class="circle-block-element_text">Инвестирование</span></li>
                            <li class="circle-block_element"><p class="circle-block-element_number">2</p><span class="circle-block-element_text">Обеспечение</span></li>
                            <li class="circle-block_element"><p class="circle-block-element_number">3</p><span class="circle-block-element_text">Становление</span></li>
                            <li class="circle-block_element"><p class="circle-block-element_number">4</p><span class="circle-block-element_text">Поддержка</span></li>
                            <li class="circle-block_element"><p class="circle-block-element_number">5</p><span class="circle-block-element_text">Доверие</span></li>
                            <li class="circle-block_element"><p class="circle-block-element_number">6</p><span class="circle-block-element_text">Уверенность</span></li>
                        </ul>
                    </div>
                    <div class="col-md-12 text-center">
                        <a href="#" class="want-button want-button__top">Хочу ZNGOcoin</a>
                    </div>
                </div>

            </div>




        </div>
    </div>
    </div> <!-- END MAIN TITLE -->


</section>
<!-- =========================
  END WELCOME SECTION
============================== -->

<!-- =========================
  START TEXTIMONIALS SECTION
============================== -->

<section class="testimonials-block">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="testimonials_testimonial">
                    <div class="testimonials_image">
                        <img src="/web/images/site-content/testimonial-photo.jpg" alt="">
                    </div>

                    <div class="testimonials_info">
                        <h5 class="testimonials_company">ZNGOCOIN - уверенность в будущем</h5>
                        <div class="testimonials_text-block">
                            <span class="small-quote small-quote__left"><i class="fa fa-quote-left"></i></span>

                            <p class="testimonials_text">
                                ООО «Завод нефтегазового оборудования» – стабильная, динамично развивающаяся компания,
                                которая является ведущим отечественным производителем запорной арматуры
                                и соединительных деталей трубопровода для газовой и нефтяной отрасли.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                            </p>
                            <span class="small-quote small-quote__right"><i class="fa fa-quote-left"></i></span>

                        </div>
                        <h5 class="testimonials_name">Иванов Иван Иванович</h5>
                    </div>
                </div>

                <div class="testimonials_testimonial">
                    <div class="testimonials_image testimonials_image__right">
                        <img src="/web/images/site-content/testimonial-photo.jpg" alt="">
                    </div>
                    <div class="testimonials_info">
                        <h5 class="testimonials_company">ZNGOCOIN - уверенность в будущем</h5>
                        <div class="testimonials_text-block testimonials_text-block__left">
                            <span class="small-quote small-quote__left"><i class="fa fa-quote-left"></i></span>

                            <p class="testimonials_text">
                                ООО «Завод нефтегазового оборудования» – стабильная, динамично развивающаяся компания,
                                которая является ведущим отечественным производителем запорной арматуры
                                и соединительных деталей трубопровода для газовой и нефтяной отрасли.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                            </p>
                            <span class="small-quote small-quote__right"><i class="fa fa-quote-left"></i></span>

                        </div>
                        <h5 class="testimonials_name">Иванов Иван Иванович</h5>
                    </div>
                </div>

                <div class="testimonials_testimonial">
                    <div class="testimonials_image">
                        <img src="/web/images/site-content/testimonial-photo.jpg" alt="">
                    </div>

                    <div class="testimonials_info">
                        <h5 class="testimonials_company">ZNGOCOIN - уверенность в будущем</h5>
                        <div class="testimonials_text-block">
                            <span class="small-quote small-quote__left"><i class="fa fa-quote-left"></i></span>

                            <p class="testimonials_text">
                                ООО «Завод нефтегазового оборудования» – стабильная, динамично развивающаяся компания,
                                которая является ведущим отечественным производителем запорной арматуры
                                и соединительных деталей трубопровода для газовой и нефтяной отрасли.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                                Технические возможности завода позволяют изготавливать детали: диаметром вращения
                                до 1000 мм и массой до 1200 кг., диаметром вращения до 3000 мм и массой до 9500 кг.
                            </p>
                            <span class="small-quote small-quote__right"><i class="fa fa-quote-left"></i></span>

                        </div>
                        <h5 class="testimonials_name">Иванов Иван Иванович</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END TESTIMONIALS SECTION
============================== -->

<!-- =========================
  START QUOTE 2 SECTION
============================== -->
<section class="quote-area  quote-3-area bg-type-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 no-padding">
                <div class="quote-area_title-block text-center">
                    <h3 class="quote-area_title text-center">Новости о компании</h3>
                </div>

                <div class="quote-area_content">
                    <div id="quote-3-slider" class="owl-carousel all-carousel owl-theme">
                        <a class="quote-single quote-3-single ">
                            <div class="quote-main-content quote-3-main-content quote-main-content_correct">
                                <p class="quote-author-details_text">
                                    Новости о Coin
                                </p>
                            </div>
                            <div class="quote-author quote-3-author">
                                <div class="quote-author-details">
                                    <p class="quote-author-details_text">Новости о Coin</p>
                                </div>
                            </div>
                        </a>
                        <?php foreach ($news as $new):?>
                        <a class="quote-single quote-3-single ">
<!--                            <div class="quote-main-content quote-3-main-content quote-main-content_correct">-->
                                <? echo Html::img('@web/'.$new->image, ['style' =>'max-width:100%;max-height: 100%;border: 1px solid black'])?>
<!--                                <p class="quote-author-details_text">-->
<!--<!--                                    -->--><?////=$new->text?>
<!--                                </p>-->
<!--                            </div>-->
                            <div class="quote-author quote-3-author">
                                <div class="quote-author-details">
                                    <p class="quote-author-details_text"><?=$new->title?></p>
                                </div>
                            </div>
                        </a>
                        <?php endforeach;?>
<!--                        <a
<class="quote-single quote-3-single ">-->
<!--                            <div class="quote-main-content quote-3-main-content quote-main-content_correct">-->
<!--                                <p class="quote-author-details_text">-->
<!--                                    Новости о Coin-->
<!--                                </p>-->
<!--                            </div>-->
<!--                            <div class="quote-author quote-3-author">-->
<!--                                <div class="quote-author-details">-->
<!--                                    <p class="quote-author-details_text">Новости о Coin</p>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </a>-->
                        <a class="quote-single quote-3-single ">
                            <div class="quote-main-content quote-3-main-content quote-main-content_correct">
                                <p class="quote-author-details_text">
                                    Новости о Coin
                                </p>
                            </div>
                            <div class="quote-author quote-3-author">
                                <div class="quote-author-details">
                                    <p class="quote-author-details_text">Новости о Coin</p>
                                </div>
                            </div>
                        </a>
                        <a class="quote-single quote-3-single ">
                            <div class="quote-main-content quote-3-main-content quote-main-content_correct">
                                <p class="quote-author-details_text">
                                    Новости о Coin
                                </p>
                            </div>
                            <div class="quote-author quote-3-author">
                                <div class="quote-author-details">
                                    <p class="quote-author-details_text">Новости о Coin</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END QUOTE 2 SECTION
============================== -->

<section class="bottom-subscribe">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">

                <div class="main-title-content text-left">
                    <form id="newsletter-signup" action="?action=signup" method="post">
                        <fieldset class="clearfix no-padding sm-text-center">
                            <div class="signup-email-block">
                                <h5 class="links_title text-center">подписаться на новости:</h5>
                                <input type="text" name="signup-email" id="signup-email" placeholder="впишите свой e-mail адрес"/>

                            </div>
                            <input type="image" src="/web/images/button-podpis.png" id="signup-button" value="Подписаться!" />
                            <p id="signup-response"></p>
                        </fieldset>
                    </form>
                </div>
            </div>

            <div class="col-sm-6 text-center">
                <div class="links_block">
                    <h5 class="links_title text-center">наши соцсети:</h5>
                    <a href="#"> <img src="/web/images/social/fb.png"/></a>
                    <a href="#"><img src="/web/images/social/g.png"/></a>
                    <a href="#"><img src="/web/images/social/in.png"/></a>
                    <a href="#"><img src="/web/images/social/ok.png"/></a>
                    <a href="#"><img src="/web/images/social/tw.png"/></a>
                    <a href="#"><img src="/web/images/social/vk.png"/></a>
                    <a href="#"><img src="/web/images/social/ytb.png"/></a>

                </div>

            </div>
            <div class="col-md-12 text-center">
                <a href="#" class="want-button">Хочу ZNGOcoin</a>
            </div>
        </div>
    </div>
</section>

<!-- =========================
  START FOOTER SECTION
============================== -->
<!--<footer class="footer-area">-->
<!---->
<!---->
<!--    <div class="footer-bottom-content-details">-->
<!--        <div class="container-fluid">-->
<!--            <div class="row">-->
<!--                <div class="footer-bottom-content">-->
<!--                    <div class="col-md-9 col-sm-9 no-padding">-->
<!--                        <div class="footer-bottom-single">-->
<!--                            <div class="footer-menu">-->
<!--                                <a href="#" class="footer_link-navigation">Главная</a>-->
<!--                                <a href="#" class="footer_link-navigation">О заводе</a>-->
<!--                                <a href="#" class="footer_link-navigation">Вопрос- ответ</a>-->
<!--                                <a href="#" class="footer_link-navigation">Как купить </a>-->
<!--                                <a href="#" class="footer_link-navigation">Сотрудничество</a>-->
<!--                                <a href="#" class="footer_link-navigation">Контакты</a>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="col-md-3 col-sm-3 no-padding">-->
<!--                        <div class="footer-bottom-single text-center">-->
<!--                            <p><a href="http://themeforest.net/user/7oroof/portfolio?ref=7oroof" class="access_link">2017 Условия пользования сайтом</a></p>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</footer>-->
<!-- =========================
  END FOOTER SECTION
============================== -->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!--<script src="/web/js/jquery-1.11.3.min.js"></script>-->
<!--<!-- RS5.0 Core JS Files -->-->
<!--<script type="text/javascript" src="/web/revolution/js/jquery.themepunch.tools.min.js?rev=5.0"></script>-->
<!--<script type="text/javascript" src="/web/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0"></script>-->
<!--<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>-->
<!--<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>-->
<!--<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.navigation.min.js"></script>-->
<!--<script type="text/javascript" src="/web/revolution/js/extensions/revolution.extension.parallax.min.js"></script>-->
<!--<script src="/web/js/bootstrap.min.js"></script>-->
<!--<script src="/web/js/menuzord.js"></script>-->
<!--<script src="/web/js/owl.carousel.min.js"></script>-->
<!--<script src="/web/js/jquery.counterup.js"></script>-->
<!--<script src="/web/js/waypoints.min.js"></script>-->
<!--<script src="/web/js/countdown.js"></script>-->
<!--<script src="/web/js/parallax.min.js"></script>-->
<!--<script src="/web/js/jquery.bootFolio.js"></script>-->
<!--<script src="/web/js/jquery.magnific-popup.js"></script>-->
<!--<script src="https://maps.googleapis.com/maps/api/js"></script>-->
<!--<script src="/web/js/jquery-ui.js"></script>-->
<!--<script src="/web/js/rev-function.js"></script>-->
<!--<script src="/web/js/smoothscroll.js"></script>-->
<!--<script src="/web/js/jquery.matchHeight.js"></script>-->
<!--<script src="/web/js/main.js"></script>-->
</body>
</html>
